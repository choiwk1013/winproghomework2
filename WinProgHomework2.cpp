// WinProgHomework2.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "WinProgHomework2.h"
#include <afxtempl.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

void problem1(int argc, TCHAR** argv);
void problem2();
void problem3(int argc, TCHAR **argv);
void problem4();

// 유일한 응용 프로그램 개체입니다.

CWinApp theApp;

using namespace std;

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// MFC를 초기화합니다. 초기화하지 못한 경우 오류를 인쇄합니다.
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: 오류 코드를 필요에 따라 수정합니다.
			_tprintf(_T("심각한 오류: MFC를 초기화하지 못했습니다.\n"));
			nRetCode = 1;
		}
		else
		{
			// TODO: 응용 프로그램의 동작은 여기에서 코딩합니다.
			_tsetlocale(LC_ALL, _T("korean"));

//			problem1(argc, argv);
//			problem2();
//			problem3(argc, argv);
			problem4();
		}
	}
	else
	{
		// TODO: 오류 코드를 필요에 따라 수정합니다.
		_tprintf(_T("심각한 오류: GetModuleHandle 실패\n"));
		nRetCode = 1;
	}

	return nRetCode;
}

void problem1(int argc, TCHAR **argv) {
//	명령행 인자로 받지않고 _tscanf()로 입력받을 경우 주석 해제
	_tscanf(_T("%s"), argv[1]);

	//	연산자의 위치를 구함
	CString exp(argv[1]);
	int pos = exp.FindOneOf(_T("+-*/"));

	//	수식에 연산자가 없을 경우
	if (pos == -1) {
		_tprintf(_T("수식이 올바르지 않습니다!\n"));
		return;
	}

	//	피연산자 2개를 뽑음
	int op_left = _wtoi((wchar_t *)(LPCTSTR)exp.Left(pos));
	int op_right = _wtoi((wchar_t *)(LPCTSTR)exp.Mid(pos + 1));

	//	계산
	double result;
	switch(exp.GetAt(pos)) {
	case '+':
		result = op_left + op_right;
		break;

	case '-':
		result = op_left - op_right;
		break;

	case '*':
		result = op_left * op_right;
		break;

	case '/':
		if (op_right == 0) {
			_tprintf(_T("0 으로 나눌 수 없습니다.\n"));
			return;
		}

		result = op_left / op_right;
		break;
	}

	_tprintf(_T("%s=%.1lf\n"), exp, result);
}

void problem2() {
#define ROW 3
#define COL 3

	int MATRIX[ROW][COL] = {
		{3, 3, 4},
		{1, 1, 2},
		{1, 2, 3}
	};

	//	변수 선언
	CPtrArray matrix;
	matrix.SetSize(ROW);

	//	삽입
	for (int i = 0; i < ROW; i++) {
		CArray<int, int&> *row = new CArray<int, int&>();
		for (int j = 0; j < COL; j++) {
			row->Add(MATRIX[i][j]);
		}
		matrix[i] = row;
	}

	//	출력
	for (int i = 0; i < ROW; i++) {
		CArray<int, int&> *row = (CArray<int, int&> *)matrix[i];
		for (int j = 0; j < COL; j++) {
			_tprintf(_T("%d "), (*row)[j]);
		}
		_tprintf(_T("\n"));
	}
}

void problem3(int argc, TCHAR **argv) {

	//	구조체 선언
	struct People {
		int height;
		int weight;

		People() {}
		People(int h, int w) {
			height = h;
			weight = w;
		}
	};

//	명령행 인자로 받지않고 _tscanf()로 입력받을 경우 주석 해제
	_tscanf(_T("%s"), argv[1]);

	CMapStringToPtr map;
	map[_T("홍길동")] = new People(172, 72);
	map[_T("박철수")] = new People(164, 67);
	map[_T("오영희")] = new People(166, 51);
	map[_T("구오성")] = new People(182, 80);
	map[_T("김순희")] = new People(159, 48);
	map[_T("최성주")] = new People(177, 69);
	map[_T("신여정")] = new People(172, 51);

	People *people;
	if (map.Lookup(argv[1], (void *&)people))
		_tprintf(_T("키 : %d, 몸무게 : %d\n"), people->height, people->weight);
	else 
		_tprintf(_T("그런 사람 없습니다.\n"));
}

void problem4() {
	//	데이터 저장
	CMapStringToString map;
	map[_T(" ")] = _T("111");			map[_T("A")] = _T("1010");
	map[_T("B")] = _T("0010011");		map[_T("C")] = _T("01001");
	map[_T("D")] = _T("01110");			map[_T("E")] = _T("110");
	map[_T("F")] = _T("0111100");		map[_T("G")] = _T("0111110");
	map[_T("H")] = _T("0010010");		map[_T("I")] = _T("1000");
	map[_T("J")] = _T("011111110");		map[_T("K")] = _T("011111111001");
	map[_T("L")] = _T("0001");			map[_T("M")] = _T("00101");
	map[_T("N")] = _T("1001");			map[_T("O")] = _T("0000");
	map[_T("P")] = _T("01000");			map[_T("Q")] = _T("0111101");
	map[_T("R")] = _T("0101");			map[_T("S")] = _T("1011");
	map[_T("T")] = _T("0110");			map[_T("U")] = _T("0011");
	map[_T("V")] = _T("001000");		map[_T("W")] = _T("011111111000");
	map[_T("X")] = _T("01111110");		map[_T("Y")] = _T("0111111111");
	map[_T("Z")] = _T("01111111101");

	//	 키, 밸류를 바꾸어 다시 저장 ==> 숫자, 문자에 상관없이 찾을 수 있게 함.
	POSITION pos = map.GetStartPosition();
	while (pos != NULL) {
		CString key, value;
		map.GetNextAssoc(pos, key, value);
		map[value] = key;
	}

	//	입력
	TCHAR input[BUFSIZ] = {0};
//	_tscanf(_T("%s"), input);
	fgetws(input, sizeof(input), stdin);	//	공백을 입력받을 수 있음

	//	대소문자 구분을 없애기 위해서
	CString str(input);
	str = str.MakeUpper();

	CString first = str.Left(1);

	if (map.PLookup(first)) {
		//	영문으로 검색하는 경우(영문인 경우 첫 글자를 찾으면 무족건 무족건 있음)
		for (int i = 0; i < str.GetLength(); i++) {
			CString key = str.Mid(i, 1);
			CString value;
			
			if(map.Lookup(key, value))
				_tprintf(_T("%s"), value);
		}
	}
	else {
		//	기호로 검색하는 경우, 검색 결과가 없는 경우
		int start = 0;	//	부분키의 시작 위치
		int length = 1;	//	부분키의 길이
		for (int i = 0; i < str.GetLength(); i++) {
			CString key = str.Mid(start, length++);	//	부분키를 구함
			CString value;

			//	부분키를 이용하여 값을 찾은 경우(유요한 부분키)
			if (map.Lookup(key, value)) {
				_tprintf(_T("%s"), value);
				start = i + 1;	//	찾은 경우 부분키의 시작위치를 바로 다음 문자부터	ex)10100101에서 1010을 찾은 경우 4번 인덱스
				length = 1;	//	찾은경우 부분키의 길이를 다시 1로
			}
				
		}
	}
	_tprintf(_T("\n"));
}